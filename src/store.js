import { createStore, applyMiddleware, combineReducers } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import form from './reducers/form';
import graph from './reducers/graph';

const Config = process.env.NODE_ENV;

const getStore = () => {
  if (Config === 'development') {
    const store = createStore(
        combineReducers({
            form,
            graph
      }),
        applyMiddleware(logger, thunk),
    );
    return store;
  } else if (Config === 'production') {
    const store = createStore(combineReducers({
        form,
        graph
    }), applyMiddleware(thunk));
    return store;
  }
};

export default getStore;
