const graph = (state = {}, action) => {
    switch (action.type) {
    case 'INITIAL_STATE': {
        return {
            ...action.state,
            showGraphLoading: false
        };
    }

    case 'DISPLAY_GRAPH_LOADING': {
        const newState = {
            ...state,
            showGraphLoading: true
        };
        return newState;
    }

    case 'HIDE_GRAPH_LOADING': {
        const newState = {
            ...state,
            showGraphLoading: false
        };
        return newState;
    }

    case 'DISPLAY_GRAPH_ERROR': {
        const newState = {
            ...state,
            error: action.error
        };
        return newState;
    }
    case 'DISPLAY_GRAPH': {
        const newState = {
            ...state,
            data: action.data
        };
        return newState;
    }
    default: return state;
    }
};

export default graph;
