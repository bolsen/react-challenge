import React from 'react';
import { connect } from 'react-redux';

import './Form.css';
import { displayName, loadGraph, displayGraphLoading } from '../../actions';

import Button from 'react-bootstrap/lib/Button';

import ows from '../../openweather';

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            errors: []
        };
        this.handleChange = this.handleChange.bind(this);
        this.submitName = this.submitName.bind(this);
    }

    handleChange(event) {
        this.setState({ name: event.target.value });
    }

    onKeyBlur(event) {
        if (event.keyCode === 13) {
            event.target.blur();
            this.submitName();
        }
    }

    submitName() {
        const errors = ows.checkInputs(this.state.name, '16');
        if (errors.length > 0) {
            this.setState({ errors });
        } else {
            this.setState({ errors: [] });
            this.props.displayNameHandler(this.state.name);
            this.props.displayGraphLoading();
            this.props.loadGraph(this.state.name);
        }
    }

    render() {
        return (
            <div>
                <h3 className="form-title">Find the 5-day temperature forecast for your city</h3>
                <div className="form">
                    <form onSubmit={e => {
                        e.preventDefault();
                        //    this.submitName();
                    }}>
                        <label htmlFor="city">Enter City:</label>
                        <input
                            autoFocus
                            name="city"
                            type="text"
                            tabIndex="1"
                            placeholder="ex. Oslo"
                            value={this.state.name}
                            onChange={this.handleChange}
                            onKeyDown={this.onKeyBlur.bind(this)}
                            className={this.state.errors.length > 0 ? "error" : ""}
                        />

                        <Button bsStyle="primary" onClick={e => this.submitName()} >Search</Button>

                    </form>
                </div>
            </div>
        );
    }

}
function mapDispatchToProps(dispatch) {
    return ({
        displayNameHandler: (name) => {
            dispatch(displayName(name));
        },

        loadGraph: (name) => {
            dispatch(loadGraph(name));
        },

        displayGraphLoading: () => {
            dispatch(displayGraphLoading());
        }
    });
}

export default connect(null, mapDispatchToProps)(Form);
