import React from 'react';
import { connect } from 'react-redux';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { Table } from 'react-bootstrap';

import ows from '../../openweather';
import Spinner from '../../graphic/spinner.png';
import './Graph.css';

function avg(list) {
    let sum = 0;
    for (let i = 0; i < list.length; i++) {
        sum += list[i];
    }
    return sum / list.length;
}

/**
 * Transform data suitable for highcharts.
 *
 * @param {Object} dataPoints Raw input from the API.
 * @returns {Object} Formatted highcharts configuration.
 */
function chartOptions(dataPoints) {
    const tempAvgs = ows.tempAverages(dataPoints);
    const dates = [];
    const max = [];
    const min = [];

    for (let date in tempAvgs) {
        dates.push(date);
        max.push(Math.ceil(avg(tempAvgs[date].max)));
        min.push(Math.floor(avg(tempAvgs[date].min)));
    }

    const result = {
        title: {
            text: ''
        },
        chart: {
            zoomType: 'x'
        },
        xAxis: {
            title: {
                text: 'Day'
            },
            categories: dates
        },
        yAxis: {
            title: {
                text: 'Average Daily Temperature in Celcius'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true,
                    format: '{y}° (C)'
                },
                enableMouseTracking: false
            }
        },

        series: [
            // max
            {
                data: max,
                color: 'red',
                name: 'Max Temperature'
            },

            // min
            {
                data: min,
                color: 'blue',
                name: 'Min Temperature'
            }
        ]
    };

    console.log(result);
    return result;
}

function dataForTable(dataPoints) {
    const tempAvgs = ows.tempAverages(dataPoints);
    const table = [];

    for (let date in tempAvgs) {
        table.push([
            date,
            Math.ceil(avg(tempAvgs[date].max)),
            Math.floor(avg(tempAvgs[date].min))
        ]);
    }

    return table;
}

const Graph = ({ name, showGraphLoading, graphData, error }) => (
    <div>
    <div
        className={graphData || error ? "graph" : "graph hide"}

    >
      {name ? <span className="title">Average Temperatures for {name}</span> : ''}
      {showGraphLoading &&
       <div className="spinner">
         <img src={Spinner} />
       </div>
      }
        {error && <div className="graph-error">{error}</div>}

      {!showGraphLoading && graphData &&
       <div>
           <HighchartsReact
               highcharts={Highcharts}
               options={chartOptions(graphData)}
           />

           <Table striped bordered condensed hover>
               <thead>
                   <tr>
                       <th>Date</th>
                       <th>Average Minimum</th>
                       <th>Average Maximum</th>
                   </tr>
               </thead>
               <tbody>
                   {dataForTable(graphData).map(row =>
                        <tr>
                            <td>{row[0]}</td>
                            <td>{row[1]}° C</td>
                            <td>{row[2]}° C</td>
                        </tr>
                   )}
               </tbody>
           </Table>
       </div>
      }
    </div>
  </div>
);

function mapStateToProps(state) {
  return ({
      name: state.form.name,
      showGraphLoading: state.graph.showGraphLoading,
      graphData: state.graph.data,
      error: state.graph.error
  });
}

export default connect(mapStateToProps)(Graph);
