import React from 'react';
import { connect } from 'react-redux';

import NavbarBs from 'react-bootstrap/lib/Navbar';

import Logo from '../../graphic/intelecy_text_logo.png';

import './Navbar.css';

const Navbar = ({ name }) => (
    <NavbarBs className="navbar-header">
      <img src={Logo} />
    </NavbarBs>
);

export default Navbar;
