import fetch from 'node-fetch';

const OWM_URL = 'https://api.openweathermap.org/data/2.5/forecast';
const APIKEY = 'fba6f4b47ac7b2943b3737cbfea3e6ae';

export function convertDate(datestr) {
    const date = new Date(datestr);
    return `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
}

/**
 * Get average max and min temperatures per date.
 *
 * @param {Object} input from openweather api
 *
 * @returns {Object} { dateString: { max: [x], min: [x]}}
 */
export function tempAverages(data) {
    let dates = {};
    let list = data.list ? data.list : data.city.list;

    for (let i = 0; i < list.length; i++) {
        let item = list[i];
        let date = convertDate(item.dt_txt);
        if (!dates[date]) {
            dates[date] = { max: [], min: [] };
        }
        dates[date].max.push(item.main.temp_max);
        dates[date].min.push(item.main.temp_min);
    }
    console.log(dates);
    return dates;
}

/**
 * Ensures that the inputs are correct.
 */
function checkInputs(city, days) {
    let errors = [];
    if (city === '' || city === null || city === undefined) {
        errors.push('City cannot be blank.');
    }
    // if (days === '' || days === null || days === undefined) {
    //     errors.push('Days cannot be blank.');
    // }
    // if (Number.isNaN(parseInt(days))) {
    //     errors.push('Days must be a number.');
    // }
    return errors;
}

function convertToQuery(queryObj) {
    const queryString = [];
    for (let key in queryObj) {
        queryString.push(`${key}=${encodeURIComponent(queryObj[key])}`);
    }
    return queryString.join('&');
}

/**
 * Get the weather forecast for a given city using the OpenWeatherMap API.
 *
 * @param {city} The city to get weather information from.
 * @param {days} The amount of days to get information for.
 *
 * @returns Promise Either resolves with JSON-formatted data,
 * rejection with input errors or rejection because of a connection issue.
 */
export function getForecast(city, days) {
    const errors = checkInputs(city, days);
    if (errors.length > 0) {
        return Promise.reject(errors);
    }

    const queryParams = {
        //cnt: encodeURIComponent(parseInt(days).toString()),
        q: city,
        APPID: APIKEY,
        units: 'metric'
    };

    const url = `${OWM_URL}?${convertToQuery(queryParams)}`;
    return fetch(url)
        .then(response => response.json())
        .catch(error => Promise.reject(error));
}

export default {
    checkInputs,
    getForecast,
    convertDate,
    tempAverages
}
