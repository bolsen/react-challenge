import fs from 'fs';
import ows from './openweather';

const EXAMPLE = JSON.parse(fs.readFileSync('./src/openweather_example.json'));

it('should render a correct date', () => {
    expect(ows.convertDate(new Date(2018, 11, 10))).toEqual('2018-11-10');
});

it('should produce a correct output for a graph', () => {
    const result = ows.tempAverages(EXAMPLE);
    console.log(result);
});

it('checks for correct inputs', () => {
    expect(ows.checkInputs('test', 10)).toEqual([]);
    expect(ows.checkInputs('foobar', 20)).toEqual([]);
});

it('should reject bad inputs', () => {
    expect(ows.checkInputs('', 0)).toEqual(['City cannot be blank.']);
    // expect(ows.checkInputs('test', 'test')).toEqual(['Days must be a number.']);
    // expect(ows.checkInputs(undefined, undefined)).toEqual(['City cannot be blank.',
    //                                                        'Days cannot be blank.',
    //                                                        'Days must be a number.']);
});

it('should get a valid response from the forecast api', done => {
    ows.getForecast('Oslo', 16).then(resp => {
        expect(resp.cod).toEqual('200');
        done();
    });
});
