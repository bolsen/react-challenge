import { getForecast } from '../openweather';

export const initialState = state => ({
    state,
    type: 'INITIAL_STATE'
});

export const displayName = name => ({
    type: 'DISPLAY_NAME',
    name
});

// Start loading graph
export const loadGraph = city => {
    return dispatch => {
        return getForecast(city).then(result => {
            if (result.cod === '200') {
                dispatch(hideGraphLoading());
                dispatch(displayGraph(result));
                dispatch(displayError(undefined));
            } else {
                dispatch(hideGraphLoading());
                dispatch(displayGraph(undefined));
                dispatch(displayError("City weather data was not found. Please try again."));
            }
        }).catch(error => {
            dispatch(hideGraphLoading());
            dispatch(displayGraph(undefined));
            dispatch(displayError('There was a connection issue. Please check your connection and try again.'));
        });
    };
};

// Promise is pending.
export const displayGraphLoading = () => ({
    type: 'DISPLAY_GRAPH_LOADING',
    showGraphLoading: true
});

// Completion of promise pending (either resolving or rejection)
export const hideGraphLoading = () => ({
    type: 'HIDE_GRAPH_LOADING',
    showGraphLoading: false
});

// Handles promise resolving of graph loading.
export const displayGraph = data => ({
    type: 'DISPLAY_GRAPH',
    data
});

// Handles promise rejection of graph loading.
export const displayError = error => ({
    type: 'DISPLAY_GRAPH_ERROR',
    error
});
