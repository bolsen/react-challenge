import React from 'react';

import Form from './components/Form';
import Graph from './components/Graph';
import Navbar from './components/Navbar';

import './App.css';

const App = () => (
    <div>
      <Navbar />
      <div className="container app">
          <Form />
          <Graph />
      </div>
    </div>
);

export default App;
